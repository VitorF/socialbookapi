package com.algaworks.socialbooks.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.algaworks.socialbooks.domain.Autor;
import com.algaworks.socialbooks.repository.AutorRepository;
import com.algaworks.socialbooks.services.exceptions.AutorNaoEncontradoException;
import com.algaworks.socialbooks.services.exceptions.AutorJaExistenteException;

@Service
public class AutorService {

	@Autowired 
	private AutorRepository autorRepository;
	
	public List<Autor> listaTodos(){
		return autorRepository.findAll();
	}
	
	
	public Autor salvar(Autor autor) {
		if(autor.getId() != null) {
			if(autorRepository.existsById(autor.getId())) {
				throw new AutorJaExistenteException("O autor informado ja possui um cadastro !");
			}
		}
		autorRepository.save(autor);
		return autor; 
	}
	
	
	
	public Autor findId(Long id) {
	  	
		try {
			return autorRepository.findById(id).get();
		}catch(Exception e) {
			throw  new AutorNaoEncontradoException("O Autor não pode ser encontrado");
		}
		
	}
	
	
}
