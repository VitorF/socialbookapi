package com.algaworks.socialbooks.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.algaworks.socialbooks.domain.Comentario;
import com.algaworks.socialbooks.domain.Livro;
import com.algaworks.socialbooks.repository.ComentarioRepository;
import com.algaworks.socialbooks.repository.LivroRepository;
import com.algaworks.socialbooks.services.exceptions.LivroNaoEncontradoException;

@Service
public class LivroService {
	@Autowired
	private LivroRepository livroRepository;
	
	@Autowired
	private ComentarioRepository comentarioRepository;
	
	
	public List<Livro> listaLivros(){
		return livroRepository.findAll();
	}
	
	public Livro findLivroId(Long id) {
		Optional<Livro> opLivro   =  livroRepository.findById(id);
		
		if(!opLivro.isPresent()) {
			throw new LivroNaoEncontradoException("O Livro Não Foi encontradao");
		}
		
		return opLivro.get();
	}
	
	
	
	public Livro salvar(Livro livro) {
		livro.setLivroId(null);
		return livroRepository.save(livro);
	}
	

	public void deletar(Long id) {
		Livro livro =  findLivroId(id);
		
		livroRepository.delete(livro);
	}
	
	
	public void atualizar(Livro livro) {
		verificaExistencia(livro);
		livroRepository.save(livro);
	}
	
	private void verificaExistencia(Livro livro ){
		findLivroId(livro.getLivroId());
	}
	
	
	public void adicionarComentario(Long livroId , Comentario comentario) {
		Livro livro = findLivroId(livroId);
		comentario.setLivro(livro);
		comentario.setData(new Date());
		comentarioRepository.save(comentario);
		
	}

	public List<Comentario> listaComentario(Long id) {
		Livro livro = findLivroId(id);
		return livro.getComentarios();
	}
		
}
