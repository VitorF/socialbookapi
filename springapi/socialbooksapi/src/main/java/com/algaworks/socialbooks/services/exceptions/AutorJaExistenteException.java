package com.algaworks.socialbooks.services.exceptions;

public class AutorJaExistenteException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public AutorJaExistenteException(String msg) {
		super(msg);
	}
	
	
	public AutorJaExistenteException(String msg,Throwable causa) {
		super(msg,causa);
	}
	
	
	
	

}
