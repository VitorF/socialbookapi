package com.algaworks.socialbooks.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DetalheErro {
	
	private long status ;
	
	private String titulo;
	
	private long timestamp;
	
	@JsonIgnore
	private String mensagemDesenvolvedor;

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getMensagemDesenvolvedor() {
		return mensagemDesenvolvedor;
	}

	public void setMensagemDesenvolvedor(String mensagemDesenvolvedor) {
		this.mensagemDesenvolvedor = mensagemDesenvolvedor;
	}
	
	
	

}
