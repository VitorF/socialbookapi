package com.algaworks.socialbooks.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.algaworks.socialbooks.domain.DetalheErro;
import com.algaworks.socialbooks.services.exceptions.AutorNaoEncontradoException;
import com.algaworks.socialbooks.services.exceptions.AutorJaExistenteException;
import com.algaworks.socialbooks.services.exceptions.LivroNaoEncontradoException;

@ControllerAdvice
public class ResourceExceptionHandler {

	@ExceptionHandler(LivroNaoEncontradoException.class)
	public ResponseEntity<DetalheErro> recuperaExceptionLivroNaoEncontratdo(LivroNaoEncontradoException e,
			HttpServletRequest request) {
		
		DetalheErro erro =  new DetalheErro();
		
		erro.setStatus(404L);
		erro.setTitulo("O Livro não foi encontrado");
		erro.setTimestamp(System.currentTimeMillis());

		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
	}
	
	
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public ResponseEntity<DetalheErro> recuperaExceptionHttpRequestMethodNotSupportedException(
			HttpRequestMethodNotSupportedException e , HttpServletRequest request){
		
		DetalheErro erro =  new DetalheErro();
		
		erro.setStatus(405L);
		erro.setTitulo("Não foi encontrado nenhum endpoint para o method adicionado");
		erro.setTimestamp(System.currentTimeMillis());

		return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(erro);
	}
	
	
	
	
	@ExceptionHandler(AutorNaoEncontradoException.class)
	public ResponseEntity<DetalheErro> recuperaExceptionAutorNaoEncontrado(AutorNaoEncontradoException e,
			HttpServletRequest request) {
		
		DetalheErro erro =  new DetalheErro();
		
		erro.setStatus(404L);
		erro.setTitulo("O Autor não foi encontrado");
		erro.setTimestamp(System.currentTimeMillis());

		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
	}
	
	
	
	@ExceptionHandler(AutorJaExistenteException.class)
	public ResponseEntity<DetalheErro> recuperaExceptionAutorNaoExistente(AutorJaExistenteException e,
			HttpServletRequest request) {
		
		DetalheErro erro =  new DetalheErro();
		
		erro.setStatus(409L);
		erro.setTitulo("O Autor ja posssui um cadastro");
		erro.setTimestamp(System.currentTimeMillis());

		return ResponseEntity.status(HttpStatus.CONFLICT).body(erro);
	}

}
