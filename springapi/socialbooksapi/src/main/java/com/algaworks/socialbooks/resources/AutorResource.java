package com.algaworks.socialbooks.resources;

import java.net.URI;
import java.net.URL;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.algaworks.socialbooks.domain.Autor;
import com.algaworks.socialbooks.services.AutorService;

@RestController
@RequestMapping("/autores")
public class AutorResource {
	@Autowired
	private  AutorService autorService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Autor>> listaAutores(){
		return ResponseEntity.ok(autorService.listaTodos());
	}
	
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> salvarAutor(@Valid @RequestBody Autor autor){
		autorService.salvar(autor);
		
		URI uri   = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(autor.getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	
	@RequestMapping(value="/{id}",method=RequestMethod.GET)
	public ResponseEntity<Autor> buscarPorId(@PathVariable("id") Long id){
		return ResponseEntity.ok(autorService.findId(id));
	}
	
	
}
