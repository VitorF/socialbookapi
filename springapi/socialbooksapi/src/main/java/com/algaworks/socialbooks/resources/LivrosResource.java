package com.algaworks.socialbooks.resources;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.algaworks.socialbooks.domain.Comentario;
import com.algaworks.socialbooks.domain.Livro;
import com.algaworks.socialbooks.repository.LivroRepository;
import com.algaworks.socialbooks.services.LivroService;
import com.algaworks.socialbooks.services.exceptions.LivroNaoEncontradoException;

@RestController
@RequestMapping("/livros")
public class LivrosResource {

	@Autowired
	private LivroService livroService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Livro>> listar() {
		return ResponseEntity.status(HttpStatus.OK).body(livroService.listaLivros());
	}

	@SuppressWarnings("static-access")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> salvar(@RequestBody Livro livro) {

		livro = livroService.salvar(livro);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().fromPath("/{id}").buildAndExpand(livro.getLivroId())
				.toUri();
		return ResponseEntity.created(uri).build();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> busca(@PathVariable("id") Long livroId) {
		Livro livro = livroService.findLivroId(livroId);
		return ResponseEntity.status(HttpStatus.OK).body(livro);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable("id") Long livroId) {
		livroService.deletar(livroId);
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizar(@RequestBody Livro livro, @PathVariable("id") Long livroId) {

		livro.setLivroId(livroId);
		livroService.atualizar(livro);

		return ResponseEntity.noContent().build();
	}
	
	@PostMapping(value = "/{id}/comentario")
	public ResponseEntity<Void> addComentario(@PathVariable("id") Long livroId , @RequestBody Comentario comentario){	
		livroService.adicionarComentario(livroId, comentario);	
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();	
		return ResponseEntity.created(uri).build();
	}
	
	@RequestMapping(value = "/{id}/comentario", method = RequestMethod.GET)
	public ResponseEntity<List<Comentario>>  listaComentarios(@PathVariable("id") Long id){
		List<Comentario> comentarios  =  livroService.listaComentario(id);
		
		return ResponseEntity.ok(comentarios);
	}

}
